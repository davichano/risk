/**
 * Created by David Paredes on 16/12/2015.
 */
$(document).ready(function () {
    $("#laoding").hide();
    $("#initUpload").on("click", function () {
        $("#laoding").show();
        $("#proyectocontrol-form").submit();
    });

    $("#openModalControl").on("click", function () {
        $('#myModal').modal('show')
    });

    $("#saveChangeControl").on("click",saveControl);

    $(".deleteControl").on("click",function(){
        deleteControl($(this).attr('id'),$(this).attr('aux'));
    });

});

function saveControl(){
    var nombre=$("#nombre").val();
    var descripcion=$("#descripcion").val();
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    //alert(nombre+"\n"+descripcion+"\n"+basePath);
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '7',n:nombre,d:descripcion},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
                console.log(datos);
            $('#control_idcontrol')
                .append($("<option></option>")
                    .attr("value",datos.id)
                    .text(datos.d.nombre));
            $('#myModal').modal('hide')
        }
    });
}

function deleteControl(id,aux){
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '8',id:id,aux:aux},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
            location.reload();
        console.log(datos);
        }
    });
}