/**
 * Created by David Paredes on 16/12/2015.
 */
$(document).ready(function () {
    $("#laoding").hide();
    $("#initUpload").on("click", function () {
        $("#laoding").show();
        $("#proyectosoftware-form").submit();
    });

    $("#openModalSoftware").on("click", function () {
        $('#myModal').modal('show')
    });

    $("#saveChangeSoftware").on("click",saveSoftware);

    $(".deleteSoftware").on("click",function(){
        deleteSoftware($(this).attr('id'),$(this).attr('aux'));
    });

});

function saveSoftware(){
    var nombre=$("#nombre").val();
    var descripcion=$("#descripcion").val();
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    //alert(nombre+"\n"+descripcion+"\n"+basePath);
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '11',n:nombre,d:descripcion},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
                console.log(datos);
            $('#software_idsoftware')
                .append($("<option></option>")
                    .attr("value",datos.id)
                    .text(datos.d.nombre));
            $('#myModal').modal('hide')
        }
    });
}

function deleteSoftware(id,aux){
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '12',id:id,aux:aux},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
            location.reload();
        console.log(datos);
        }
    });
}