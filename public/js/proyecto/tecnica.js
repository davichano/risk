/**
 * Created by David Paredes on 16/12/2015.
 */
$(document).ready(function () {
    $("#laoding").hide();
    $("#initUpload").on("click", function () {
        $("#laoding").show();
        $("#proyectotecnica-form").submit();
    });

    $("#openModalTecnica").on("click", function () {
        $('#myModal').modal('show')
    });

    $("#saveChangeTecnica").on("click",saveTecnica);

    $(".deleteTecnica").on("click",function(){
        deleteTecnica($(this).attr('id'),$(this).attr('aux'));
    });

});

function saveTecnica(){
    var nombre=$("#nombre").val();
    var descripcion=$("#descripcion").val();
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    //alert(nombre+"\n"+descripcion+"\n"+basePath);
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '1',n:nombre,d:descripcion},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
                console.log(datos);
            $('#tecnica_idtecnica')
                .append($("<option></option>")
                    .attr("value",datos.id)
                    .text(datos.d.nombre));
            $('#myModal').modal('hide')
        }
    });
}

function deleteTecnica(id,aux){
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '2',id:id,aux:aux},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
            location.reload();
        console.log(datos);
        }
    });
}