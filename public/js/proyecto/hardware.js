/**
 * Created by David Paredes on 16/12/2015.
 */
$(document).ready(function () {
    $("#laoding").hide();
    $("#initUpload").on("click", function () {
        $("#laoding").show();
        $("#proyectohardware-form").submit();
    });

    $("#openModalHardware").on("click", function () {
        $('#myModal').modal('show')
    });

    $("#saveChangeHardware").on("click",saveHardware);

    $(".deleteHardware").on("click",function(){
        deleteHardware($(this).attr('id'),$(this).attr('aux'));
    });

});

function saveHardware(){
    var nombre=$("#nombre").val();
    var descripcion=$("#descripcion").val();
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    //alert(nombre+"\n"+descripcion+"\n"+basePath);
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '9',n:nombre,d:descripcion},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
                console.log(datos);
            $('#hardware_idhardware')
                .append($("<option></option>")
                    .attr("value",datos.id)
                    .text(datos.d.nombre));
            $('#myModal').modal('hide')
        }
    });
}

function deleteHardware(id,aux){
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '10',id:id,aux:aux},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
            location.reload();
        console.log(datos);
        }
    });
}