/**
 * Created by David Paredes on 16/12/2015.
 */
$(document).ready(function () {
    $("#laoding").hide();
    $("#initUpload").on("click", function () {
        $("#laoding").show();
        $("#proyectoriesgo-form").submit();
    });

    $("#openModalRiesgo").on("click", function () {
        $('#myModal').modal('show')
    });

    $("#saveChangeRiesgo").on("click",saveRiesgo);

    $(".deleteRiesgo").on("click",function(){
        deleteRiesgo($(this).attr('id'),$(this).attr('aux'));
    });

});

function saveRiesgo(){
    var nombre=$("#nombre").val();
    var descripcion=$("#descripcion").val();
    var causa=$("#causa").val();
    var tipex=$("#tipex").val();
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '3',n:nombre,d:descripcion,c:causa,t:tipex},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
            console.log(datos);
            $('#riesgo_idriesgo')
                .append($("<option></option>")
                    .attr("value",datos.id)
                    .text(datos.d.nombre));
            $('#myModal').modal('hide')
        }
    });
}

function deleteRiesgo(id,aux){
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '4',id:id,aux:aux},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
            location.reload();
            console.log(datos);
        }
    });
}