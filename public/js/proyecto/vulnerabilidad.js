/**
 * Created by David Paredes on 16/12/2015.
 */
$(document).ready(function () {
    $("#laoding").hide();
    $("#initUpload").on("click", function () {
        $("#laoding").show();
        $("#proyectovulnerabilidad-form").submit();
    });

    $("#openModalVulnerabilidad").on("click", function () {
        $('#myModal').modal('show')
    });

    $("#saveChangeVulnerabilidad").on("click",saveVulnerabilidad);

    $(".deleteVulnerabilidad").on("click",function(){
        deleteVulnerabilidad($(this).attr('id'),$(this).attr('aux'));
    });

});

function saveVulnerabilidad(){
    var nombre=$("#nombre").val();
    var descripcion=$("#descripcion").val();
    var causa=$("#causa").val();
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '5',n:nombre,d:descripcion,c:causa},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
            console.log(datos);
            $('#vulnerabilidad_idvulnerabilidad')
                .append($("<option></option>")
                    .attr("value",datos.id)
                    .text(datos.d.nombre));
            $('#myModal').modal('hide')
        }
    });
}

function deleteVulnerabilidad(id,aux){
    var basePath=$("#basePath").val();
    var id=$("#id").val();
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: {f: '6',id:id,aux:aux},
        url: basePath+"/proyecto/ajax/"+id,
        success: function (datos) {
            location.reload();
            console.log(datos);
        }
    });
}