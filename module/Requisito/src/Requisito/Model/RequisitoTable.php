<?php
namespace Requisito\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class RequisitoTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getRequisito($id)
    {
        $rowset = $this->tableGateway->select(array('idrequisito' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No se encontro datos en: $id");
        }

        return $row;
    }

    public function enableRequisito($id)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('idrequisito' => $id)
        );
    }

    public function disableRequisito($id)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('idrequisito' => $id)
        );
    }

    public function saveRequisito(Requisito $requisito)
    {
        $data = array(
            'nombre' => $requisito->nombre,
            'descripcion' => $requisito->descripcion,
            'estado' => $requisito->estado,
        );

        $id = (int)$requisito->idrequisito;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getRequisito($id)) {
                $this->tableGateway->update(
                    $data, array('idrequisito' => $id)
                );
            } else {
                throw new \Exception('Requisito no existe');
            }
        }

        return $id;
    }

    public function deleteRequisito($id)
    {
        $this->tableGateway->delete(array('idrequisito' => $id));
    }


}

?>