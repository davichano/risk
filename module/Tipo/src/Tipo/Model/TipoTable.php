<?php
namespace Tipo\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class TipoTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getTipo($id)
    {
        $rowset = $this->tableGateway->select(array('idtipo' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No se encontro datos en: $id");
        }

        return $row;
    }

    public function enableTipo($id)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('idtipo' => $id)
        );
    }

    public function disableTipo($id)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('idtipo' => $id)
        );
    }

    public function saveTipo(Tipo $tipo)
    {
        $data = array(
            'nombre' => $tipo->nombre,
            'descripcion' => $tipo->descripcion,
            'estado' => $tipo->estado,
        );

        $id = (int)$tipo->idtipo;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getTipo($id)) {
                $this->tableGateway->update(
                    $data, array('idtipo' => $id)
                );
            } else {
                throw new \Exception('Tipo no existe');
            }
        }

        return $id;
    }

    public function deleteTipo($id)
    {
        $this->tableGateway->delete(array('idtipo' => $id));
    }


}

?>