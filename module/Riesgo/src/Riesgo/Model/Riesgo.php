<?php
namespace Riesgo\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 */
class Riesgo implements InputFilterAwareInterface
{
    public $idriesgo;
    public $nombre;
    public $descripcion;
    public $causa;
    public $estado;
    public $tipo_idtipo;
    protected $inputFilter;


    public function exchangeArray($data)
    {
        $this->idriesgo = (!empty($data['idriesgo']))
            ? $data['idriesgo']
            : null;
        $this->nombre = (!empty($data['nombre'])) ? $data['nombre'] : null;
        $this->descripcion = (!empty($data['descripcion']))
            ? $data['descripcion'] : null;
        $this->causa = (!empty($data['causa']))
            ? $data['causa'] : null;
        $this->estado = (!empty($data['estado'])) ? $data['estado'] : null;
        $this->tipo_idtipo = (!empty($data['tipo_idtipo'])) ? $data['tipo_idtipo'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(
                array(
                    'name'     => 'idriesgo',
                    'required' => false,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'nombre',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'El nombre tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'El nombre no puede superar los 50 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese un nombre"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'descripcion',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 250,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La descripción tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La descripción no puede superar los 250 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una descripción"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'causa',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La causa tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La causa no puede superar los 250 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una causa"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'tipo_idtipo',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Seleccione un tipo de riesgo"
                                ),
                            )
                        ),
                    ),
                )
            );
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }


}
