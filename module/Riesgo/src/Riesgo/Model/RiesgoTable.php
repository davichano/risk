<?php
namespace Riesgo\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class RiesgoTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function  fetchAllInner(){
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'idriesgo',
                'nombre',
                'descripcion',
                'estado',
                'causa',
            )
        );
        $sqlSelect->join(
            'tipo',
            'riesgo.tipo_idtipo = tipo.idtipo',
            array(
                'tipo' => 'nombre',
            )
        );
        $sqlSelect->order('idriesgo DESC');
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getRiesgo($id)
    {
        $rowset = $this->tableGateway->select(array('idriesgo' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No se encontro datos en: $id");
        }

        return $row;
    }

    public function enableRiesgo($id)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('idriesgo' => $id)
        );
    }

    public function disableRiesgo($id)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('idriesgo' => $id)
        );
    }

    public function saveRiesgo(Riesgo $riesgo)
    {
        $data = array(
            'nombre'      => $riesgo->nombre,
            'descripcion' => $riesgo->descripcion,
            'causa' => $riesgo->causa,
            'tipo_idtipo' => $riesgo->tipo_idtipo,
            'estado'      => $riesgo->estado,
        );

        $id = (int)$riesgo->idriesgo;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getRiesgo($id)) {
                $this->tableGateway->update(
                    $data, array('idriesgo' => $id)
                );
            } else {
                throw new \Exception('Riesgo no existe');
            }
        }

        return $id;
    }

    public function deleteRiesgo($id)
    {
        $this->tableGateway->delete(array('idriesgo' => $id));
    }


}

?>