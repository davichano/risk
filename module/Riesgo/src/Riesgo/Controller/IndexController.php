<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Riesgo\Controller;

use Riesgo\Form\RiesgoForm;
use Riesgo\Model\Riesgo;
use Tipo\Model\Tipo;
use Zend\Db\Sql\Sql;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    protected $regionTable;
    protected $tipoTable;

    public function indexAction()
    {
        return new ViewModel(
            array(
                'data' => $this->getRiesgoTable()->fetchAllInner(),
            )
        );
    }

    public function addAction()
    {
        $form = new RiesgoForm('riesgo-form');
        $form->get('estado')->setValue('1');
        $tipo=$this->getTipoTable()->fetchAll();
        $data=array();
        foreach($tipo as $value){
            $data[$value->idtipo]=$value->nombre;
        }
        $form->get('tipo_idtipo')->setAttribute('options', $data);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $riesgo = new Riesgo();
            $form->setInputFilter($riesgo->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $riesgo->exchangeArray($form->getData());
                $this->getRiesgoTable()
                    ->saveRiesgo(
                        $riesgo
                    );

                return $this->redirect()->toRoute(
                    'riesgo',
                    array('action' => 'index')
                );
            }
        }

        return array('form' => $form);

    }

    public function editAction()
    {

        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'riesgo', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $riesgo = $this->getRiesgoTable()->getRiesgo($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'riesgo', array(
                    'action' => 'index'
                )
            );
        }

        $form = new RiesgoForm();
        $tipo=$this->getTipoTable()->fetchAll();
        $data=array();
        foreach($tipo as $value){
            $data[$value->idtipo]=$value->nombre;
        }
        $form->get('tipo_idtipo')->setAttribute('options', $data);
        $form->bind($riesgo);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($riesgo->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $riesgo->idriesgo = $id;
                $this->getRiesgoTable()->saveRiesgo($riesgo);

//                 return new JsonModel(array("t"=>$riesgo));
                return $this->redirect()->toRoute('riesgo');
            }
        }

        return array(
            'id'   => $id,
            'form' => $form,
        );

    }

    public function updateAction()
    {

        if ($this->request->isXmlHttpRequest()) {
            $f = $this->request->getPost('f');

            switch ($f) {
                case "1":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getRiesgoTable()->disableRiesgo($value);
                    }
                    break;
                case "2":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getRiesgoTable()->enableRiesgo($value);
                    }
                    break;

                default:
                    # code...
                    break;
            }

            return new JsonModel(array('d' => $f));
            // return $s;
        } else {
            return new JsonModel(array('d' => '1'));
        }/*
      echo "<script>alert('SSS');</script>";
        return new JsonModel($this->getRiesgoTable()->fetchAll());*/


    }


    public function getRiesgoTable()
    {
        if (!$this->regionTable) {
            $sm = $this->getServiceLocator();
            $this->regionTable = $sm->get('Riesgo\Model\RiesgoTable');
        }

        return $this->regionTable;
    }

    public function getTipoTable()
    {
        if (!$this->tipoTable) {
            $sm = $this->getServiceLocator();
            $this->tipoTable = $sm->get('Tipo\Model\TipoTable');
        }

        return $this->tipoTable;
    }


    public function resultAction()
    {
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select()
            ->from('provincia')
            ->where(array('IdProvincia' => 1))
            ->order('nombre DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        //$datos=$result->toArray();
        $toRet = array('nom'   => "David", 'ape' => "Paredes",
                       'datos' => $result);

        return new ViewModel($toRet);

    }


}
