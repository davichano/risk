<?php
namespace Riesgo\Form;

use Zend\Form\Form;

/**
 *
 */
class RiesgoForm extends Form
{

    function __construct($name = null)
    {
        parent::__construct($name = null);

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'riesgo-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(
            array(
                'name' => 'idriesgo',
                'type' => 'Hidden',
            )
        );

        $this->add(
            array(
                'name'       => 'nombre',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ingrese el nombre',
                    'id'             => 'nombre',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Nombre con el que identificaremos la riesgo',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'descripcion',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Ingrese una descripción',
                    'id'          => 'descripcion',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Es necesario conocer la riesgo a detalle',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'causa',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Ingrese una causa',
                    'id'          => 'causa',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Es necesario conocer los motivos que generan el riesgo',
                ),
            )
        );
        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'tipo_idtipo',
                'options'    => array(
                    'disable_inarray_validator' => true, // <-- disable
                    'empty_option'              => 'Seleccione una opción ...',
//                    'value_options'             => array(
//                        '0' => 'Partido',
//                        '1' => 'Movimiento',
//                        '2' => 'Agrupación',
//                    ),
                ),
                'attributes' => array(
                    'id'    => 'tipo_idtipo',
                    'class' => 'form-control',
                ),
            )
        );
        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Radio',
                'name'       => 'estado',
                'options'    => array(
                    'label_attributes' => array(
                        'class' => 'radio-inline',
                    ),
                    'value_options'    => array(
                        '0' => 'Inactivo',
                        '1' => 'Activo',
                    ),
                ),
                'attributes' => array(
                    'id' => 'estado',
                ),
            )
        );
    }
}