<?php 
namespace Tecnica\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

/**
 * 
 */
 class TecnicaTable
 {
 	  protected $tableGateway;
    protected $dbAdapter;

 	public function __construct(TableGateway $tableGateway)
     {
         $this->tableGateway = $tableGateway;
     }

 	public function fetchAll()
     {
         $resultSet = $this->tableGateway->select();
         return $resultSet;
     }
     public function getTecnica($id){
     	 $rowset = $this->tableGateway->select(array('idtecnica' => $id));
         $row = $rowset->current();
         if (!$row) {
             throw new \Exception("No se encontro datos en: $id");
         }
         return $row;
     }

     public function enableTecnica($id){
        $this->tableGateway->update(array("estado"=>"1"),array('idtecnica' => $id));
     }

    public function disableTecnica($id){
        $this->tableGateway->update(array("estado"=>"0"),array('idtecnica' => $id));
     }

     public function saveTecnica(Tecnica $tecnica)
     {
         $data = array(
             'nombre'  => $tecnica->nombre,
             'descripcion' => $tecnica->descripcion,
             'estado' => $tecnica->estado,
         );

         $id = (int) $tecnica->idtecnica;
         if ($id == 0) {
             $this->tableGateway->insert($data);
             $id=$this->tableGateway->lastInsertValue;
         } else {
             if ($this->getTecnica($id)) {
                 $this->tableGateway->update($data, array('idtecnica' => $id));
             } else {
                 throw new \Exception('Región no existe');
             }
         }
         return $id;
     }

     public function deleteTecnica($id)
     {
         $this->tableGateway->delete(array('idtecnica' => $id));
     }


 } 

 ?>