<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Tecnica\Controller;

use Tecnica\Form\TecnicaForm;
use Tecnica\Model\Tecnica;
use Zend\Db\Sql\Sql;
use Zend\Form\FormInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    protected $regionTable;

    public function indexAction()
    {
        return new ViewModel(
            array(
                'data' => $this->getTecnicaTable()->fetchAll(),
            )
        );
    }

    public function addAction()
    {
        $form = new TecnicaForm('tecnica-form');
        $form->get('estado')->setValue('1');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $tecnica = new Tecnica();
            $form->setInputFilter($tecnica->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $tecnica->exchangeArray($form->getData());
                $this->getTecnicaTable()
                    ->saveTecnica(
                        $tecnica
                    );

                return $this->redirect()->toRoute(
                    'tecnica',
                    array('action' => 'index')
                );
            }
        }

        return array('form' => $form);

    }

    public function editAction()
    {

        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'tecnica', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $tecnica = $this->getTecnicaTable()->getTecnica($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'tecnica', array(
                    'action' => 'index'
                )
            );
        }

        $form = new TecnicaForm();
        $form->bind($tecnica);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($tecnica->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $tecnica->idtecnica=$id;
                $this->getTecnicaTable()->saveTecnica($tecnica);
//                 return new JsonModel(array("t"=>$tecnica));
                return $this->redirect()->toRoute('tecnica');
            }
        }

        return array(
            'id'   => $id,
            'form' => $form,
        );

    }

    public function updateAction()
    {

        if ($this->request->isXmlHttpRequest()) {
            $f = $this->request->getPost('f');

            switch ($f) {
                case "1":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getTecnicaTable()->disableTecnica($value);
                    }
                    break;
                case "2":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getTecnicaTable()->enableTecnica($value);
                    }
                    break;

                default:
                    # code...
                    break;
            }

            return new JsonModel(array('d' => $f));
            // return $s;
        } else {
            return new JsonModel(array('d' => '1'));
        }/*
      echo "<script>alert('SSS');</script>";
        return new JsonModel($this->getTecnicaTable()->fetchAll());*/


    }


    public function getTecnicaTable()
    {
        if (!$this->regionTable) {
            $sm = $this->getServiceLocator();
            $this->regionTable = $sm->get('Tecnica\Model\TecnicaTable');
        }

        return $this->regionTable;
    }


    public function resultAction()
    {
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select()
            ->from('provincia')
            ->where(array('IdProvincia' => 1))
            ->order('nombre DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        //$datos=$result->toArray();
        $toRet = array('nom'   => "David", 'ape' => "Paredes",
                       'datos' => $result);

        return new ViewModel($toRet);

    }


}
