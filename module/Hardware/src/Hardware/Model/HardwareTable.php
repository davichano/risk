<?php
namespace Hardware\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class HardwareTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getHardware($id)
    {
        $rowset = $this->tableGateway->select(array('idhardware' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No se encontro datos en: $id");
        }

        return $row;
    }

    public function enableHardware($id)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('idhardware' => $id)
        );
    }

    public function disableHardware($id)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('idhardware' => $id)
        );
    }

    public function saveHardware(Hardware $hardware)
    {
        $data = array(
            'nombre' => $hardware->nombre,
            'descripcion' => $hardware->descripcion,
            'estado' => $hardware->estado,
        );

        $id = (int)$hardware->idhardware;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getHardware($id)) {
                $this->tableGateway->update(
                    $data, array('idhardware' => $id)
                );
            } else {
                throw new \Exception('Hardware no existe');
            }
        }

        return $id;
    }

    public function deleteHardware($id)
    {
        $this->tableGateway->delete(array('idhardware' => $id));
    }


}

?>