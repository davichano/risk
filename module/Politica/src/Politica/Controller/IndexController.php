<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Politica\Controller;

use Politica\Form\PoliticaForm;
use Politica\Model\Politica;
use Zend\Db\Sql\Sql;
use Zend\Form\FormInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    protected $regionTable;

    public function indexAction()
    {
        return new ViewModel(
            array(
                'data' => $this->getPoliticaTable()->fetchAll(),
            )
        );
    }

    public function addAction()
    {
        $form = new PoliticaForm('politica-form');
        $form->get('estado')->setValue('1');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $politica = new Politica();
            $form->setInputFilter($politica->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $politica->exchangeArray($form->getData());
                $this->getPoliticaTable()
                    ->savePolitica(
                        $politica
                    );

                return $this->redirect()->toRoute(
                    'politica',
                    array('action' => 'index')
                );
            }
        }

        return array('form' => $form);

    }

    public function editAction()
    {

        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'politica', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $politica = $this->getPoliticaTable()->getPolitica($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'politica', array(
                    'action' => 'index'
                )
            );
        }

        $form = new PoliticaForm();
        $form->bind($politica);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($politica->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $politica->idpolitica=$id;
                $this->getPoliticaTable()->savePolitica($politica);
//                 return new JsonModel(array("t"=>$politica));
                return $this->redirect()->toRoute('politica');
            }
        }

        return array(
            'id'   => $id,
            'form' => $form,
        );

    }

    public function updateAction()
    {

        if ($this->request->isXmlHttpRequest()) {
            $f = $this->request->getPost('f');

            switch ($f) {
                case "1":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getPoliticaTable()->disablePolitica($value);
                    }
                    break;
                case "2":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getPoliticaTable()->enablePolitica($value);
                    }
                    break;

                default:
                    # code...
                    break;
            }

            return new JsonModel(array('d' => $f));
            // return $s;
        } else {
            return new JsonModel(array('d' => '1'));
        }/*
      echo "<script>alert('SSS');</script>";
        return new JsonModel($this->getPoliticaTable()->fetchAll());*/


    }


    public function getPoliticaTable()
    {
        if (!$this->regionTable) {
            $sm = $this->getServiceLocator();
            $this->regionTable = $sm->get('Politica\Model\PoliticaTable');
        }

        return $this->regionTable;
    }


    public function resultAction()
    {
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select()
            ->from('provincia')
            ->where(array('IdProvincia' => 1))
            ->order('nombre DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        //$datos=$result->toArray();
        $toRet = array('nom'   => "David", 'ape' => "Paredes",
                       'datos' => $result);

        return new ViewModel($toRet);

    }


}
