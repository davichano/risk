<?php
namespace Politica\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class PoliticaTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getPolitica($id)
    {
        $rowset = $this->tableGateway->select(array('idpolitica' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No se encontro datos en: $id");
        }

        return $row;
    }

    public function enablePolitica($id)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('idpolitica' => $id)
        );
    }

    public function disablePolitica($id)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('idpolitica' => $id)
        );
    }

    public function savePolitica(Politica $politica)
    {
        $data = array(
            'nombre' => $politica->nombre,
            'descripcion' => $politica->descripcion,
            'estado' => $politica->estado,
        );

        $id = (int)$politica->idpolitica;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getPolitica($id)) {
                $this->tableGateway->update(
                    $data, array('idpolitica' => $id)
                );
            } else {
                throw new \Exception('Politica no existe');
            }
        }

        return $id;
    }

    public function deletePolitica($id)
    {
        $this->tableGateway->delete(array('idpolitica' => $id));
    }


}

?>