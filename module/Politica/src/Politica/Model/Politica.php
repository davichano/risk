<?php
namespace Politica\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 */
class Politica implements InputFilterAwareInterface
{
    public $idpolitica;
    public $nombre;
    public $descripcion;
    public $estado;
    protected $inputFilter;


    public function exchangeArray($data)
    {
        $this->idpolitica = (!empty($data['idpolitica']))
            ? $data['idpolitica']
            : null;
        $this->nombre = (!empty($data['nombre'])) ? $data['nombre'] : null;
        $this->descripcion = (!empty($data['descripcion']))
            ? $data['descripcion'] : null;
        $this->estado = (!empty($data['estado'])) ? $data['estado'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(
                array(
                    'name'     => 'idpolitica',
                    'required' => false,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'nombre',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'El nombre tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'El nombre no puede superar los 50 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese un nombre"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'descripcion',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 250,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La descripción tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La descripción no puede superar los 250 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una descripción"
                                ),
                            )
                        ),
                    ),
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }


}
