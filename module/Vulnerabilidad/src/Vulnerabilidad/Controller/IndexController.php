<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Vulnerabilidad\Controller;

use Vulnerabilidad\Form\VulnerabilidadForm;
use Vulnerabilidad\Model\Vulnerabilidad;
use Zend\Db\Sql\Sql;
use Zend\Form\FormInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    protected $regionTable;

    public function indexAction()
    {
        return new ViewModel(
            array(
                'data' => $this->getVulnerabilidadTable()->fetchAll(),
            )
        );
    }

    public function addAction()
    {
        $form = new VulnerabilidadForm('vulnerabilidad-form');
        $form->get('estado')->setValue('1');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $vulnerabilidad = new Vulnerabilidad();
            $form->setInputFilter($vulnerabilidad->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $vulnerabilidad->exchangeArray($form->getData());
                $this->getVulnerabilidadTable()
                    ->saveVulnerabilidad(
                        $vulnerabilidad
                    );

                return $this->redirect()->toRoute(
                    'vulnerabilidad',
                    array('action' => 'index')
                );
            }
        }

        return array('form' => $form);

    }

    public function editAction()
    {

        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'vulnerabilidad', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $vulnerabilidad = $this->getVulnerabilidadTable()->getVulnerabilidad($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'vulnerabilidad', array(
                    'action' => 'index'
                )
            );
        }

        $form = new VulnerabilidadForm();
        $form->bind($vulnerabilidad);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($vulnerabilidad->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $vulnerabilidad->idvulnerabilidad=$id;
                $this->getVulnerabilidadTable()->saveVulnerabilidad($vulnerabilidad);
//                 return new JsonModel(array("t"=>$vulnerabilidad));
                return $this->redirect()->toRoute('vulnerabilidad');
            }
        }

        return array(
            'id'   => $id,
            'form' => $form,
        );

    }

    public function updateAction()
    {

        if ($this->request->isXmlHttpRequest()) {
            $f = $this->request->getPost('f');

            switch ($f) {
                case "1":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getVulnerabilidadTable()->disableVulnerabilidad($value);
                    }
                    break;
                case "2":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getVulnerabilidadTable()->enableVulnerabilidad($value);
                    }
                    break;

                default:
                    # code...
                    break;
            }

            return new JsonModel(array('d' => $f));
            // return $s;
        } else {
            return new JsonModel(array('d' => '1'));
        }/*
      echo "<script>alert('SSS');</script>";
        return new JsonModel($this->getVulnerabilidadTable()->fetchAll());*/


    }


    public function getVulnerabilidadTable()
    {
        if (!$this->regionTable) {
            $sm = $this->getServiceLocator();
            $this->regionTable = $sm->get('Vulnerabilidad\Model\VulnerabilidadTable');
        }

        return $this->regionTable;
    }


    public function resultAction()
    {
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select()
            ->from('provincia')
            ->where(array('IdProvincia' => 1))
            ->order('nombre DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        //$datos=$result->toArray();
        $toRet = array('nom'   => "David", 'ape' => "Paredes",
                       'datos' => $result);

        return new ViewModel($toRet);

    }


}
