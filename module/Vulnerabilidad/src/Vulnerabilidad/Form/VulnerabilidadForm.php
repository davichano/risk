<?php
namespace Vulnerabilidad\Form;

use Zend\Form\Form;

/**
 *
 */
class VulnerabilidadForm extends Form
{

    function __construct($name = null)
    {
        parent::__construct($name = null);

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'vulnerabilidad-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(
            array(
                'name' => 'idvulnerabilidad',
                'type' => 'Hidden',
            )
        );

        $this->add(
            array(
                'name'       => 'nombre',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ingrese el nombre',
                    'id'             => 'nombre',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Nombre con el que identificaremos la vulnerabilidad',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'descripcion',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Ingrese una descripción',
                    'id'          => 'descripcion',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Es necesario conocer la vulnerabilidad a detalle',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'causa',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Ingrese la causa de la vulnerabilidad',
                    'id'          => 'causa',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Saber las causas de la vulnerabilidad nos podrá ayudar a buscar soluciones',
                ),
            )
        );
        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Radio',
                'name'       => 'estado',
                'options'    => array(
                    'label_attributes' => array(
                        'class' => 'radio-inline',
                    ),
                    'value_options'    => array(
                        '0' => 'Inactivo',
                        '1' => 'Activo',
                    ),
                ),
                'attributes' => array(
                    'id' => 'estado',
                ),
            )
        );
    }
}