<?php
namespace Vulnerabilidad\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class VulnerabilidadTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getVulnerabilidad($id)
    {
        $rowset = $this->tableGateway->select(array('idvulnerabilidad' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No se encontro datos en: $id");
        }

        return $row;
    }

    public function enableVulnerabilidad($id)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('idvulnerabilidad' => $id)
        );
    }

    public function disableVulnerabilidad($id)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('idvulnerabilidad' => $id)
        );
    }

    public function saveVulnerabilidad(Vulnerabilidad $vulnerabilidad)
    {
        $data = array(
            'nombre' => $vulnerabilidad->nombre,
            'descripcion' => $vulnerabilidad->descripcion,
            'estado' => $vulnerabilidad->estado,
            'causa' => $vulnerabilidad->causa,
        );

        $id = (int)$vulnerabilidad->idvulnerabilidad;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getVulnerabilidad($id)) {
                $this->tableGateway->update(
                    $data, array('idvulnerabilidad' => $id)
                );
            } else {
                throw new \Exception('Vulnerabilidad no existe');
            }
        }

        return $id;
    }

    public function deleteVulnerabilidad($id)
    {
        $this->tableGateway->delete(array('idvulnerabilidad' => $id));
    }


}

?>