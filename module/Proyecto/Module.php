<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Proyecto;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Proyecto\Model\Proyecto;
use Proyecto\Model\ProyectoTable;
use Proyecto\Model\Proyectotecnica;
use Proyecto\Model\ProyectotecnicaTable;
use Proyecto\Model\Proyectocontrol;
use Proyecto\Model\ProyectocontrolTable;
use Proyecto\Model\Proyectohardware;
use Proyecto\Model\ProyectohardwareTable;
use Proyecto\Model\Proyectosoftware;
use Proyecto\Model\ProyectosoftwareTable;
use Proyecto\Model\Proyectorequisito;
use Proyecto\Model\ProyectorequisitoTable;
use Proyecto\Model\Proyectoriesgo;
use Proyecto\Model\ProyectoriesgoTable;
use Proyecto\Model\Proyectousuario;
use Proyecto\Model\ProyectousuarioTable;
use Proyecto\Model\Proyectovulnerabilidad;
use Proyecto\Model\ProyectovulnerabilidadTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
     //   $view=$this
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

 // Add this method:
     public function getServiceConfig()
     {
         return array(
             'factories' => array(
                 'Proyecto\Model\ProyectoTable' =>  function($sm) {
                     $tableGateway = $sm->get('ProyectoTableGateway');
                     $table = new ProyectoTable($tableGateway);
                     return $table;
                 },
                 'ProyectoTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Proyecto());
                     return new TableGateway('proyecto', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Proyecto\Model\ProyectotecnicaTable' =>  function($sm) {
                     $tableGateway = $sm->get('ProyectotecnicaTableGateway');
                     $table = new ProyectotecnicaTable($tableGateway);
                     return $table;
                 },
                 'ProyectotecnicaTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Proyectotecnica());
                     return new TableGateway('proyecto_tecnica', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Proyecto\Model\ProyectocontrolTable' =>  function($sm) {
                     $tableGateway = $sm->get('ProyectocontrolTableGateway');
                     $table = new ProyectocontrolTable($tableGateway);
                     return $table;
                 },
                 'ProyectocontrolTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Proyectocontrol());
                     return new TableGateway('proyecto_control', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Proyecto\Model\ProyectohardwareTable' =>  function($sm) {
                     $tableGateway = $sm->get('ProyectohardwareTableGateway');
                     $table = new ProyectohardwareTable($tableGateway);
                     return $table;
                 },
                 'ProyectohardwareTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Proyectohardware());
                     return new TableGateway('proyecto_hardware', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Proyecto\Model\ProyectosoftwareTable' =>  function($sm) {
                     $tableGateway = $sm->get('ProyectosoftwareTableGateway');
                     $table = new ProyectosoftwareTable($tableGateway);
                     return $table;
                 },
                 'ProyectosoftwareTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Proyectosoftware());
                     return new TableGateway('proyecto_software', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Proyecto\Model\ProyectorequisitoTable' =>  function($sm) {
                     $tableGateway = $sm->get('ProyectorequisitoTableGateway');
                     $table = new ProyectorequisitoTable($tableGateway);
                     return $table;
                 },
                 'ProyectorequisitoTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Proyectorequisito());
                     return new TableGateway('proyecto_requisito', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Proyecto\Model\ProyectoriesgoTable' =>  function($sm) {
                     $tableGateway = $sm->get('ProyectoriesgoTableGateway');
                     $table = new ProyectoriesgoTable($tableGateway);
                     return $table;
                 },
                 'ProyectoriesgoTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Proyectoriesgo());
                     return new TableGateway('proyecto_riesgo', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Proyecto\Model\ProyectousuarioTable' =>  function($sm) {
                     $tableGateway = $sm->get('ProyectousuarioTableGateway');
                     $table = new ProyectousuarioTable($tableGateway);
                     return $table;
                 },
                 'ProyectousuarioTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Proyectousuario());
                     return new TableGateway('proyecto_usuario', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Proyecto\Model\ProyectovulnerabilidadTable' =>  function($sm) {
                     $tableGateway = $sm->get('ProyectovulnerabilidadTableGateway');
                     $table = new ProyectovulnerabilidadTable($tableGateway);
                     return $table;
                 },
                 'ProyectovulnerabilidadTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Proyectovulnerabilidad());
                     return new TableGateway('proyecto_vulnerabilidad', $dbAdapter, null, $resultSetPrototype);
                 },
             ),
         );
     }


}
