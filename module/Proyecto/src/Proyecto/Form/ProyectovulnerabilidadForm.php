<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:34 PM
 */

namespace Proyecto\Form;

use Zend\Form\Form;


class ProyectovulnerabilidadForm extends Form
{
    function __construct($name = null)
    {
        parent::__construct($name = null);

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'proyectovulnerabilidad-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(
            array(
                'name' => 'proyecto_idproyecto',
                'type' => 'Hidden',
            )
        );
        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'vulnerabilidad_idvulnerabilidad',
                'options'    => array(
                    'disable_inarray_validator' => true, // <-- disable
                    'empty_option'              => 'Seleccione una opción ...',
                ),
                'attributes' => array(
                    'id'    => 'vulnerabilidad_idvulnerabilidad',
                    'class' => 'form-control',
                ),
            )
        );
        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'impacto',
                'options'    => array(
                    'disable_inarray_validator' => true, // <-- disable
                    'empty_option'              => 'Seleccione una opción ...',
                                        'value_options'             => array(
                                            '1' => 'Bajo',
                                            '2' => 'Medio',
                                            '3' => 'Alto',
                                        ),
                ),
                'attributes' => array(
                    'id'    => 'impacto',
                    'class' => 'form-control',
                ),
            )
        );
    }
}