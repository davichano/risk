<?php
namespace Proyecto\Form;

use Zend\Form\Form;

/**
 *
 */
class ProyectoForm extends Form
{

    function __construct($name = null)
    {
        parent::__construct($name = null);

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'proyecto-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(
            array(
                'name' => 'idproyecto',
                'type' => 'Hidden',
            )
        );

        $this->add(
            array(
                'name'       => 'nombre',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ingrese el nombre',
                    'id'             => 'nombre',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Nombre con el que identificaremos la proyecto',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'descripcion',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Ingrese una descripción',
                    'id'          => 'descripcion',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Es necesario conocer la proyecto a detalle',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'mision',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Ingrese la mision',
                    'id'          => 'mision',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Misión que cumplirá el sistema dentro de la organización. Eje: los procesos realizados por el sistema',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'datacritica',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Criticidad de la data',
                    'id'          => 'datacritica',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => '¿Qué tan importante es la data manejada en el sistema para su organización?',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'almacenamiento',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Información sobre el almacenamiento',
                    'id'          => 'almacenamiento',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Protección de almacenamiento de información del sistema',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'seguridad',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Controles operacionales y técnicos',
                    'id'          => 'seguridad',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Controles operacionales y técnicos utilizados para el sistema. Ej: Políticas de seguridad del data center, auditorías, etc.',
                ),
            )
        );

        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Radio',
                'name'       => 'estado',
                'options'    => array(
                    'label_attributes' => array(
                        'class' => 'radio-inline',
                    ),
                    'value_options'    => array(
                        '0' => 'Inactivo',
                        '1' => 'Activo',
                    ),
                ),
                'attributes' => array(
                    'id' => 'estado',
                ),
            )
        );
    }
}