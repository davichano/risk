<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:34 PM
 */

namespace Proyecto\Form;

use Zend\Form\Form;


class ProyectosoftwareForm extends Form
{
    function __construct($name = null)
    {
        parent::__construct($name = null);

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'proyectosoftware-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(
            array(
                'name' => 'proyecto_idproyecto',
                'type' => 'Hidden',
            )
        );
        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'software_idsoftware',
                'options'    => array(
                    'disable_inarray_validator' => true, // <-- disable
                    'empty_option'              => 'Seleccione una opción ...',
                ),
                'attributes' => array(
                    'id'    => 'software_idsoftware',
                    'class' => 'form-control',
                ),
            )
        );
    }
}