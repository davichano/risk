<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Proyecto\Controller;

use Proyecto\Form\ProyectoForm;
use Proyecto\Form\ProyectoriesgoForm;
use Proyecto\Form\ProyectousuarioForm;
use Proyecto\Form\ProyectotecnicaForm;
use Proyecto\Form\ProyectocontrolForm;
use Proyecto\Form\ProyectohardwareForm;
use Proyecto\Form\ProyectosoftwareForm;
use Proyecto\Form\ProyectorequisitoForm;
use Proyecto\Form\ProyectovulnerabilidadForm;
use Proyecto\Model\Proyecto;
use Proyecto\Model\Proyectoriesgo;
use Proyecto\Model\Proyectousuario;
use Proyecto\Model\Proyectotecnica;
use Proyecto\Model\Proyectocontrol;
use Proyecto\Model\Proyectohardware;
use Proyecto\Model\Proyectosoftware;
use Proyecto\Model\Proyectorequisito;
use Proyecto\Model\Proyectovulnerabilidad;
use Tecnica\Model\Tecnica;
use Control\Model\Control;
use Hardware\Model\Hardware;
use Software\Model\Software;
use Requisito\Model\Requisito;
use Riesgo\Model\Riesgo;
use Politica\Model\Politica;
use Usuario\Model\Usuario;
use Vulnerabilidad\Model\Vulnerabilidad;
use Zend\Db\Sql\Sql;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    protected $regionTable;
    protected $proyectotecnicaTable;
    protected $proyectocontrolTable;
    protected $proyectohardwareTable;
    protected $proyectosoftwareTable;
    protected $proyectorequisitoTable;
    protected $proyectoriesgoTable;
    protected $proyectousuarioTable;
    protected $proyectovulnerabilidadTable;
    protected $tecnicaTable;
    protected $controlTable;
    protected $hardwareTable;
    protected $softwareTable;
    protected $requisitoTable;
    protected $riesgoTable;
    protected $usuarioTable;
    protected $politicaTable;
    protected $vulnerabilidadTable;
    protected $tipoTable;

    public function indexAction()
    {
        return new ViewModel(
            array(
                'data' => $this->getProyectoTable()->fetchAll(),
            )
        );
    }

    public function addAction()
    {
        $form = new ProyectoForm('proyecto-form');
        $form->get('estado')->setValue('1');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proyecto = new Proyecto();
            $form->setInputFilter($proyecto->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $proyecto->exchangeArray($form->getData());
               $id=$this->getProyectoTable()
                    ->saveProyecto(
                        $proyecto
                    );

                return $this->redirect()->toRoute(
                    'proyecto',
                    array('action' => 'hardware','id'=>$id)
                );
            }
        }

        return array('form' => $form);

    }

    public function editAction()
    {

        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }

        $form = new ProyectoForm('proyecto-form');
        $form->bind($proyecto);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($proyecto->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $proyecto->idproyecto=$id;
                $id=$this->getProyectoTable()
                    ->saveProyecto(
                        $proyecto
                    );
                return $this->redirect()->toRoute('proyecto');
            }
        }
//        if ($request->isPost()) {
//            $form->setInputFilter($proyecto->getInputFilter());
//            $post = array_merge_recursive(
//                $request->getPost()->toArray(),
//                $request->getFiles()->toArray()
//            );
//            $form->setData($post);
//            if ($form->isValid()) {
//                $proyecto->exchangeArray($form->getData());
//                $id=$this->getProyectoTable()
//                    ->saveProyecto(
//                        $proyecto
//                    );
//
//                return $this->redirect()->toRoute(
//                    'proyecto',
//                    array('action' => 'hardware','id'=>$id)
//                );
//            }
//        }

        return array(
            'id'   => $id,
            'form' => $form,
        );

    }

    public function tecnicaAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        $form = new ProyectotecnicaForm();
        $tipo = $this->getTecnicaTable()->fetchAll();
        $data = array();
        foreach ($tipo as $value) {
            $data[$value->idtecnica] = $value->nombre;
        }
        $form->get('tecnica_idtecnica')->setAttribute('options', $data);
        $form->get('proyecto_idproyecto')->setValue($id);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $ptecnica = new Proyectotecnica();
            $form->setInputFilter($ptecnica->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $hoy = date("Y-m-d");
                $ptecnica->exchangeArray($form->getData());
                $ptecnica->estado = '1';
                $ptecnica->fecha = $hoy;
                $this->getProyectotecnicaTable()
                    ->saveProyectoTecnica(
                        $ptecnica
                    );

                return $this->redirect()->toRoute(
                    'proyecto',
                    array('action' => 'tecnica', 'id' => $proyecto->idproyecto)
                );
            }
        }
        $data = $this->getProyectotecnicaTable()->historyProject($id);

        return new ViewModel(
            array('form' => $form, 'proyecto' => $proyecto, 'data' => $data)
        );
    }
    
    public function controlAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        $form = new ProyectocontrolForm();
        $tipo = $this->getControlTable()->fetchAll();
        $data = array();
        foreach ($tipo as $value) {
            $data[$value->idcontrol] = $value->nombre;
        }
        $form->get('control_idcontrol')->setAttribute('options', $data);
        $form->get('proyecto_idproyecto')->setValue($id);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pcontrol = new Proyectocontrol();
            $form->setInputFilter($pcontrol->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $hoy = date("Y-m-d");
                $pcontrol->exchangeArray($form->getData());
                $pcontrol->estado = '1';
                $pcontrol->fecha = $hoy;
                $this->getProyectocontrolTable()
                    ->saveProyectoControl(
                        $pcontrol
                    );

                return $this->redirect()->toRoute(
                    'proyecto',
                    array('action' => 'control', 'id' => $proyecto->idproyecto)
                );
            }
        }
        $data = $this->getProyectocontrolTable()->historyProject($id);

        return new ViewModel(
            array('form' => $form, 'proyecto' => $proyecto, 'data' => $data)
        );
    }
    
    public function hardwareAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        $form = new ProyectohardwareForm();
        $tipo = $this->getHardwareTable()->fetchAll();
        $data = array();
        foreach ($tipo as $value) {
            $data[$value->idhardware] = $value->nombre;
        }
        $form->get('hardware_idhardware')->setAttribute('options', $data);
        $form->get('proyecto_idproyecto')->setValue($id);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $phardware = new Proyectohardware();
            $form->setInputFilter($phardware->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $hoy = date("Y-m-d");
                $phardware->exchangeArray($form->getData());
                $phardware->estado = '1';
                $phardware->fecha = $hoy;
                $this->getProyectohardwareTable()
                    ->saveProyectoHardware(
                        $phardware
                    );

                return $this->redirect()->toRoute(
                    'proyecto',
                    array('action' => 'hardware', 'id' => $proyecto->idproyecto)
                );
            }
        }
        $data = $this->getProyectohardwareTable()->historyProject($id);

        return new ViewModel(
            array('form' => $form, 'proyecto' => $proyecto, 'data' => $data)
        );
    }
    
    public function softwareAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        $form = new ProyectosoftwareForm();
        $tipo = $this->getSoftwareTable()->fetchAll();
        $data = array();
        foreach ($tipo as $value) {
            $data[$value->idsoftware] = $value->nombre;
        }
        $form->get('software_idsoftware')->setAttribute('options', $data);
        $form->get('proyecto_idproyecto')->setValue($id);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $psoftware = new Proyectosoftware();
            $form->setInputFilter($psoftware->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $hoy = date("Y-m-d");
                $psoftware->exchangeArray($form->getData());
                $psoftware->estado = '1';
                $psoftware->fecha = $hoy;
                $this->getProyectosoftwareTable()
                    ->saveProyectoSoftware(
                        $psoftware
                    );

                return $this->redirect()->toRoute(
                    'proyecto',
                    array('action' => 'software', 'id' => $proyecto->idproyecto)
                );
            }
        }
        $data = $this->getProyectosoftwareTable()->historyProject($id);

        return new ViewModel(
            array('form' => $form, 'proyecto' => $proyecto, 'data' => $data)
        );
    }
    
    public function requisitoAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        $form = new ProyectorequisitoForm();
        $tipo = $this->getRequisitoTable()->fetchAll();
        $data = array();
        foreach ($tipo as $value) {
            $data[$value->idrequisito] = $value->nombre;
        }
        $form->get('requisito_idrequisito')->setAttribute('options', $data);
        $form->get('proyecto_idproyecto')->setValue($id);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $prequisito = new Proyectorequisito();
            $form->setInputFilter($prequisito->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $hoy = date("Y-m-d");
                $prequisito->exchangeArray($form->getData());
                $prequisito->estado = '1';
                $prequisito->fecha = $hoy;
                $this->getProyectorequisitoTable()
                    ->saveProyectoRequisito(
                        $prequisito
                    );

                return $this->redirect()->toRoute(
                    'proyecto',
                    array('action' => 'requisito', 'id' => $proyecto->idproyecto)
                );
            }
        }
        $data = $this->getProyectorequisitoTable()->historyProject($id);

        return new ViewModel(
            array('form' => $form, 'proyecto' => $proyecto, 'data' => $data)
        );
    }
    
    public function riesgoAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        $form = new ProyectoriesgoForm();
        $tipo = $this->getRiesgoTable()->fetchAll();
        $data = array();
        foreach ($tipo as $value) {
            $data[$value->idriesgo] = $value->nombre;
        }
        $form->get('riesgo_idriesgo')->setAttribute('options', $data);
        $form->get('proyecto_idproyecto')->setValue($id);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $priesgo = new Proyectoriesgo();
            $form->setInputFilter($priesgo->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $hoy = date("Y-m-d");
                $priesgo->exchangeArray($form->getData());
                $priesgo->estado = '1';
                $priesgo->fecha = $hoy;
                $this->getProyectoriesgoTable()
                    ->saveProyectoRiesgo(
                        $priesgo
                    );

                return $this->redirect()->toRoute(
                    'proyecto',
                    array('action' => 'riesgo', 'id' => $proyecto->idproyecto)
                );
            }
        }
        $data = $this->getProyectoriesgoTable()->historyProject($id);
        $t = $this->getTipoTable()->fetchAll();

        return new ViewModel(
            array('form' => $form, 'proyecto' => $proyecto, 'data' => $data,
                  't'    => $t)
        );
    }
    
    public function usuarioAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        $form = new ProyectousuarioForm();
        $tipo = $this->getUsuarioTable()->fetchAll();
        $data = array();
        foreach ($tipo as $value) {
            $data[$value->idusuario] = $value->nombre;
        }
        $poli = $this->getpoliticaTable()->fetchAll();
        $datab = array();
        foreach ($poli as $value) {
            $datab[$value->idpolitica] = $value->nombre;
        }
        $form->get('usuario_idusuario')->setAttribute('options', $data);
        $form->get('politica_idpolitica')->setAttribute('options', $datab);
        $form->get('proyecto_idproyecto')->setValue($id);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pusuario = new Proyectousuario();
            $form->setInputFilter($pusuario->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $hoy = date("Y-m-d");
                $pusuario->exchangeArray($form->getData());
                $pusuario->estado = '1';
                $pusuario->fecha = $hoy;
                $this->getProyectousuarioTable()
                    ->saveProyectoUsuario(
                        $pusuario
                    );

                return $this->redirect()->toRoute(
                    'proyecto',
                    array('action' => 'usuario', 'id' => $proyecto->idproyecto)
                );
            }
        }
        $data = $this->getProyectousuarioTable()->historyProject($id);
        $t = $this->getTipoTable()->fetchAll();

        return new ViewModel(
            array('form' => $form, 'proyecto' => $proyecto, 'data' => $data,
                  't'    => $t)
        );
    }
    
    public function vulnerabilidadAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        $form = new ProyectovulnerabilidadForm();
        $tipo = $this->getVulnerabilidadTable()->fetchAll();
        $data = array();
        foreach ($tipo as $value) {
            $data[$value->idvulnerabilidad] = $value->nombre;
        }
        $form->get('vulnerabilidad_idvulnerabilidad')->setAttribute('options', $data);
        $form->get('proyecto_idproyecto')->setValue($id);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pvulnerabilidad = new Proyectovulnerabilidad();
            $form->setInputFilter($pvulnerabilidad->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $hoy = date("Y-m-d");
                $pvulnerabilidad->exchangeArray($form->getData());
                $pvulnerabilidad->estado = '1';
                $pvulnerabilidad->fecha = $hoy;
                $this->getProyectovulnerabilidadTable()
                    ->saveProyectoVulnerabilidad(
                        $pvulnerabilidad
                    );

                return $this->redirect()->toRoute(
                    'proyecto',
                    array('action' => 'vulnerabilidad', 'id' => $proyecto->idproyecto)
                );
            }
        }
        $data = $this->getProyectovulnerabilidadTable()->historyProject($id);
        $t = $this->getTipoTable()->fetchAll();

        return new ViewModel(
            array('form' => $form, 'proyecto' => $proyecto, 'data' => $data,
                  't'    => $t)
        );
    }


    public function ajaxAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        if ($this->request->isXmlHttpRequest()) {
            $f = (int)$this->request->getPost('f');

            switch ($f) {
                case 1:
                    $nombre = $this->request->getPost('n');
                    $descripcion = $this->request->getPost('d');
                    $tecnica = new Tecnica();
                    $tecnica->estado = '1';
                    $tecnica->nombre = $nombre;
                    $tecnica->descripcion = $descripcion;
                    $id = $this->getTecnicaTable()->saveTecnica($tecnica);

                    return new JsonModel(array('id' => $id, 'd' => $tecnica));

                    break;
                case 2:
                    $proyecto = $this->request->getPost('id');
                    $tecnica = $this->request->getPost('aux');
                    $ptec = $this->getProyectotecnicaTable()
                        ->getProyectoTecnica($proyecto, $tecnica);
                    $ptec->estado = '0';
                    $pt = new Proyectotecnica();
                    $pt->proyecto_idproyecto = $ptec->proyecto_idproyecto;
                    $pt->tecnica_idtecnica = $ptec->tecnica_idtecnica;
                    $pt->estado = '0';
                    $pt->fecha = $ptec->fecha;
                    $id = $this->getProyectotecnicaTable()->saveProyectoTecnica(
                        $pt
                    );

                    return new JsonModel(array('id' => $id, 'd' => $pt));
                    break;
                case 3:
                    $nombre = $this->request->getPost('n');
                    $descripcion = $this->request->getPost('d');
                    $causa = $this->request->getPost('c');
                    $tipo = $this->request->getPost('t');
                    $riesgo = new Riesgo();
                    $riesgo->estado = '1';
                    $riesgo->nombre = $nombre;
                    $riesgo->descripcion = $descripcion;
                    $riesgo->causa = $causa;
                    $riesgo->tipo_idtipo = $tipo;
                    $id = $this->getRiesgoTable()->saveRiesgo($riesgo);

                    return new JsonModel(array('id' => $id, 'd' => $riesgo));

                    break;
                case 4:
                    $proyecto = $this->request->getPost('id');
                    $riesgo = $this->request->getPost('aux');
                    $ptec = $this->getProyectoriesgoTable()
                        ->getProyectoRiesgo($proyecto, $riesgo);
                    $pt = new Proyectoriesgo();
                    $pt->proyecto_idproyecto = $ptec->proyecto_idproyecto;
                    $pt->riesgo_idriesgo = $ptec->riesgo_idriesgo;
                    $pt->estado = '0';
                    $pt->fecha = $ptec->fecha;
                    $pt->valor = $ptec->valor;
                    $id = $this->getProyectoriesgoTable()->saveProyectoRiesgo(
                        $pt
                    );

                    return new JsonModel(array('id' => $id, 'd' => $pt));
                    break;
                case 5:
                    $nombre = $this->request->getPost('n');
                    $descripcion = $this->request->getPost('d');
                    $causa = $this->request->getPost('c');
                    $vulnerabilidad = new Vulnerabilidad();
                    $vulnerabilidad->estado = '1';
                    $vulnerabilidad->nombre = $nombre;
                    $vulnerabilidad->descripcion = $descripcion;
                    $vulnerabilidad->causa = $causa;
                    $id = $this->getVulnerabilidadTable()->saveVulnerabilidad($vulnerabilidad);

                    return new JsonModel(array('id' => $id, 'd' => $vulnerabilidad));

                    break;
                case 6:
                    $proyecto = $this->request->getPost('id');
                    $vulnerabilidad = $this->request->getPost('aux');
                    $ptec = $this->getProyectovulnerabilidadTable()
                        ->getProyectoVulnerabilidad($proyecto, $vulnerabilidad);
                    $pt = new Proyectovulnerabilidad();
                    $pt->proyecto_idproyecto = $ptec->proyecto_idproyecto;
                    $pt->vulnerabilidad_idvulnerabilidad = $ptec->vulnerabilidad_idvulnerabilidad;
                    $pt->estado = '0';
                    $pt->fecha = $ptec->fecha;
                    $pt->impacto = $ptec->impacto;
                    $id = $this->getProyectovulnerabilidadTable()->saveProyectoVulnerabilidad(
                        $pt
                    );

                    return new JsonModel(array('id' => $id, 'd' => $pt));
                    break;
                case 7:
                    $nombre = $this->request->getPost('n');
                    $descripcion = $this->request->getPost('d');
                    $control = new Control();
                    $control->estado = '1';
                    $control->nombre = $nombre;
                    $control->descripcion = $descripcion;
                    $id = $this->getControlTable()->saveControl($control);

                    return new JsonModel(array('id' => $id, 'd' => $control));

                    break;
                case 8:
                    $proyecto = $this->request->getPost('id');
                    $control = $this->request->getPost('aux');
                    $ptec = $this->getProyectocontrolTable()
                        ->getProyectoControl($proyecto, $control);
                    $ptec->estado = '0';
                    $pt = new Proyectocontrol();
                    $pt->proyecto_idproyecto = $ptec->proyecto_idproyecto;
                    $pt->control_idcontrol = $ptec->control_idcontrol;
                    $pt->estado = '0';
                    $pt->fecha = $ptec->fecha;
                    $id = $this->getProyectocontrolTable()->saveProyectoControl(
                        $pt
                    );

                    return new JsonModel(array('id' => $id, 'd' => $pt));
                    break;
                case 9:
                    $nombre = $this->request->getPost('n');
                    $descripcion = $this->request->getPost('d');
                    $hardware = new Hardware();
                    $hardware->estado = '1';
                    $hardware->nombre = $nombre;
                    $hardware->descripcion = $descripcion;
                    $id = $this->getHardwareTable()->saveHardware($hardware);

                    return new JsonModel(array('id' => $id, 'd' => $hardware));

                    break;
                case 10:
                    $proyecto = $this->request->getPost('id');
                    $hardware = $this->request->getPost('aux');
                    $ptec = $this->getProyectohardwareTable()
                        ->getProyectoHardware($proyecto, $hardware);
                    $ptec->estado = '0';
                    $pt = new Proyectohardware();
                    $pt->proyecto_idproyecto = $ptec->proyecto_idproyecto;
                    $pt->hardware_idhardware = $ptec->hardware_idhardware;
                    $pt->estado = '0';
                    $pt->fecha = $ptec->fecha;
                    $id = $this->getProyectohardwareTable()->saveProyectoHardware(
                        $pt
                    );

                    return new JsonModel(array('id' => $id, 'd' => $pt));
                    break;
                case 11:
                    $nombre = $this->request->getPost('n');
                    $descripcion = $this->request->getPost('d');
                    $software = new Software();
                    $software->estado = '1';
                    $software->nombre = $nombre;
                    $software->descripcion = $descripcion;
                    $id = $this->getSoftwareTable()->saveSoftware($software);

                    return new JsonModel(array('id' => $id, 'd' => $software));

                    break;
                case 12:
                    $proyecto = $this->request->getPost('id');
                    $software = $this->request->getPost('aux');
                    $ptec = $this->getProyectosoftwareTable()
                        ->getProyectoSoftware($proyecto, $software);
                    $ptec->estado = '0';
                    $pt = new Proyectosoftware();
                    $pt->proyecto_idproyecto = $ptec->proyecto_idproyecto;
                    $pt->software_idsoftware = $ptec->software_idsoftware;
                    $pt->estado = '0';
                    $pt->fecha = $ptec->fecha;
                    $id = $this->getProyectosoftwareTable()->saveProyectoSoftware(
                        $pt
                    );

                    return new JsonModel(array('id' => $id, 'd' => $pt));
                    break;
                case 13:
                    $nombre = $this->request->getPost('n');
                    $descripcion = $this->request->getPost('d');
                    $requisito = new Requisito();
                    $requisito->estado = '1';
                    $requisito->nombre = $nombre;
                    $requisito->descripcion = $descripcion;
                    $id = $this->getRequisitoTable()->saveRequisito($requisito);

                    return new JsonModel(array('id' => $id, 'd' => $requisito));

                    break;
                case 14:
                    $proyecto = $this->request->getPost('id');
                    $requisito = $this->request->getPost('aux');
                    $ptec = $this->getProyectorequisitoTable()
                        ->getProyectoRequisito($proyecto, $requisito);
                    $ptec->estado = '0';
                    $pt = new Proyectorequisito();
                    $pt->proyecto_idproyecto = $ptec->proyecto_idproyecto;
                    $pt->requisito_idrequisito = $ptec->requisito_idrequisito;
                    $pt->estado = '0';
                    $pt->fecha = $ptec->fecha;
                    $id = $this->getProyectorequisitoTable()->saveProyectoRequisito(
                        $pt
                    );

                    return new JsonModel(array('id' => $id, 'd' => $pt));
                    break;

                case 15:
                    $nombre = $this->request->getPost('n');
                    $descripcion = $this->request->getPost('d');
                    $usuario = new Usuario();
                    $usuario->estado = '1';
                    $usuario->nombre = $nombre;
                    $usuario->descripcion = $descripcion;
                    $id = $this->getUsuarioTable()->saveUsuario($usuario);

                    return new JsonModel(array('id' => $id, 'd' => $usuario));

                    break;
                case 16:
                    $proyecto = $this->request->getPost('id');
                    $usuario = $this->request->getPost('aux');
                    $politica = $this->request->getPost('pol');
                    $ptec = $this->getProyectousuarioTable()
                        ->getProyectoUsuario($proyecto, $usuario,$politica);
                    $pt = new Proyectousuario();
                    $pt->proyecto_idproyecto = $ptec->proyecto_idproyecto;
                    $pt->usuario_idusuario = $ptec->usuario_idusuario;
                    $pt->politica_idpolitica = $ptec->politica_idpolitica;
                    $pt->estado = '0';
                    $pt->fecha = $ptec->fecha;
                    $id = $this->getProyectousuarioTable()->saveProyectoUsuario(
                        $pt
                    );
                    return new JsonModel(array('id' => $id, 'd' => $pt));
                    break;

                case 17:
                    $nombre = $this->request->getPost('n');
                    $descripcion = $this->request->getPost('d');
                    $politica = new Politica();
                    $politica->estado = '1';
                    $politica->nombre = $nombre;
                    $politica->descripcion = $descripcion;
                    $id = $this->getPoliticaTable()->savePolitica($politica);

                    return new JsonModel(array('id' => $id, 'd' => $politica));

                    break;
                default:
                    # code...
                    break;
            }

            return new JsonModel(array('d' => $f));
        }
    }

    public function resultAction()
    {
        date_default_timezone_set("America/Lima");
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $proyecto = $this->getProyectoTable()->getProyecto($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'proyecto', array(
                    'action' => 'index'
                )
            );
        }
        $tecnica=$this->getProyectotecnicaTable()->historyProject($proyecto->idproyecto);
        $vulnerabilidad=$this->getProyectovulnerabilidadTable()->historyProject($proyecto->idproyecto);
        $control=$this->getProyectocontrolTable()->historyProject($proyecto->idproyecto);
        $software=$this->getProyectosoftwareTable()->historyProject($proyecto->idproyecto);
        $hardware=$this->getProyectohardwareTable()->historyProject($proyecto->idproyecto);
        $requisito=$this->getProyectorequisitoTable()->historyProject($proyecto->idproyecto);
        $usuario=$this->getProyectousuarioTable()->historyProject($proyecto->idproyecto);
        $riesgo=$this->getProyectoriesgoTable()->historyProject($proyecto->idproyecto);
        return new ViewModel(array(
            "tecnica"=>$tecnica,
            "vulnerabilidad"=>$vulnerabilidad,
            "control"=>$control,
            "software"=>$software,
            "hardware"=>$hardware,
            "requisito"=>$requisito,
            "usuario"=>$usuario,
            "riesgo"=>$riesgo,
            "proyecto"=>$proyecto
        ));

    }


    public function getProyectoTable()
    {
        if (!$this->regionTable) {
            $sm = $this->getServiceLocator();
            $this->regionTable = $sm->get('Proyecto\Model\ProyectoTable');
        }

        return $this->regionTable;
    }

    public function getProyectotecnicaTable()
    {
        if (!$this->proyectotecnicaTable) {
            $sm = $this->getServiceLocator();
            $this->proyectotecnicaTable = $sm->get(
                'Proyecto\Model\ProyectotecnicaTable'
            );
        }

        return $this->proyectotecnicaTable;
    }
    
    public function getProyectocontrolTable()
    {
        if (!$this->proyectocontrolTable) {
            $sm = $this->getServiceLocator();
            $this->proyectocontrolTable = $sm->get(
                'Proyecto\Model\ProyectocontrolTable'
            );
        }

        return $this->proyectocontrolTable;
    }
    
    public function getProyectohardwareTable()
    {
        if (!$this->proyectohardwareTable) {
            $sm = $this->getServiceLocator();
            $this->proyectohardwareTable = $sm->get(
                'Proyecto\Model\ProyectohardwareTable'
            );
        }

        return $this->proyectohardwareTable;
    }
    
    public function getProyectosoftwareTable()
    {
        if (!$this->proyectosoftwareTable) {
            $sm = $this->getServiceLocator();
            $this->proyectosoftwareTable = $sm->get(
                'Proyecto\Model\ProyectosoftwareTable'
            );
        }

        return $this->proyectosoftwareTable;
    }
    
    public function getProyectorequisitoTable()
    {
        if (!$this->proyectorequisitoTable) {
            $sm = $this->getServiceLocator();
            $this->proyectorequisitoTable = $sm->get(
                'Proyecto\Model\ProyectorequisitoTable'
            );
        }

        return $this->proyectorequisitoTable;
    }

    public function getProyectoriesgoTable()
    {
        if (!$this->proyectoriesgoTable) {
            $sm = $this->getServiceLocator();
            $this->proyectoriesgoTable = $sm->get(
                'Proyecto\Model\ProyectoriesgoTable'
            );
        }

        return $this->proyectoriesgoTable;
    }

    public function getProyectousuarioTable()
    {
        if (!$this->proyectousuarioTable) {
            $sm = $this->getServiceLocator();
            $this->proyectousuarioTable = $sm->get(
                'Proyecto\Model\ProyectousuarioTable'
            );
        }

        return $this->proyectousuarioTable;
    }
    public function getProyectovulnerabilidadTable()
    {
        if (!$this->proyectovulnerabilidadTable) {
            $sm = $this->getServiceLocator();
            $this->proyectovulnerabilidadTable = $sm->get(
                'Proyecto\Model\ProyectovulnerabilidadTable'
            );
        }

        return $this->proyectovulnerabilidadTable;
    }

    public function getTecnicaTable()
    {
        if (!$this->tecnicaTable) {
            $sm = $this->getServiceLocator();
            $this->tecnicaTable = $sm->get('Tecnica\Model\TecnicaTable');
        }

        return $this->tecnicaTable;
    }
    
    public function getControlTable()
    {
        if (!$this->controlTable) {
            $sm = $this->getServiceLocator();
            $this->controlTable = $sm->get('Control\Model\ControlTable');
        }

        return $this->controlTable;
    }
    public function getHardwareTable()
    {
        if (!$this->hardwareTable) {
            $sm = $this->getServiceLocator();
            $this->hardwareTable = $sm->get('Hardware\Model\HardwareTable');
        }

        return $this->hardwareTable;
    }
    public function getSoftwareTable()
    {
        if (!$this->softwareTable) {
            $sm = $this->getServiceLocator();
            $this->softwareTable = $sm->get('Software\Model\SoftwareTable');
        }

        return $this->softwareTable;
    }

    public function getRequisitoTable()
    {
        if (!$this->requisitoTable) {
            $sm = $this->getServiceLocator();
            $this->requisitoTable = $sm->get('Requisito\Model\RequisitoTable');
        }

        return $this->requisitoTable;
    }

    public function getRiesgoTable()
    {
        if (!$this->riesgoTable) {
            $sm = $this->getServiceLocator();
            $this->riesgoTable = $sm->get('Riesgo\Model\RiesgoTable');
        }

        return $this->riesgoTable;
    }

    public function getUsuarioTable()
    {
        if (!$this->usuarioTable) {
            $sm = $this->getServiceLocator();
            $this->usuarioTable = $sm->get('Usuario\Model\UsuarioTable');
        }

        return $this->usuarioTable;
    }
    
    public function getpoliticaTable()
    {
        if (!$this->politicaTable) {
            $sm = $this->getServiceLocator();
            $this->politicaTable = $sm->get('politica\Model\politicaTable');
        }

        return $this->politicaTable;
    }
    public function getVulnerabilidadTable()
    {
        if (!$this->vulnerabilidadTable) {
            $sm = $this->getServiceLocator();
            $this->vulnerabilidadTable = $sm->get('Vulnerabilidad\Model\VulnerabilidadTable');
        }

        return $this->vulnerabilidadTable;
    }

    public function getTipoTable()
    {
        if (!$this->tipoTable) {
            $sm = $this->getServiceLocator();
            $this->tipoTable = $sm->get('Tipo\Model\TipoTable');
        }

        return $this->tipoTable;
    }




}
