<?php
namespace Proyecto\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 */
class Proyecto implements InputFilterAwareInterface
{
    public $idproyecto;
    public $nombre;
    public $descripcion;
    public $mision;
    public $datacritica;
    public $almacenamiento;
    public $seguridad;
    public $estado;
    protected $inputFilter;


    public function exchangeArray($data)
    {
        $this->idproyecto = (!empty($data['idproyecto'])) ? $data['idproyecto'] : null;
        $this->nombre = (!empty($data['nombre'])) ? $data['nombre'] : null;
        $this->descripcion = (!empty($data['descripcion'])) ? $data['descripcion'] : null;
        $this->mision = (!empty($data['mision'])) ? $data['mision'] : null;
        $this->datacritica = (!empty($data['datacritica'])) ? $data['datacritica'] : null;
        $this->almacenamiento = (!empty($data['almacenamiento'])) ? $data['almacenamiento'] : null;
        $this->seguridad = (!empty($data['seguridad'])) ? $data['seguridad'] : null;
        $this->estado = (!empty($data['estado'])) ? $data['estado'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(
                array(
                    'name'     => 'idproyecto',
                    'required' => false,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'nombre',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'El nombre tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'El nombre no puede superar los 100 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese un nombre"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'descripcion',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 250,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La descripción tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La descripción no puede superar los 250 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una descripción"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'datacritica',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 250,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La data critica tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La data critica no puede superar los 250 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una misión"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'mision',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 250,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La misión tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La misión no puede superar los 250 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una misión"
                                ),
                            )
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'almacenamiento',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 300,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'El almacenamiento tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'El almacenamiento no puede superar los 300 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese información respecto al almacenamiento"
                                ),
                            )
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'seguridad',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 300,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La información sobre seguridad tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La información sobre seguridad no puede superar los 300 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese información sobre seguridad"
                                ),
                            )
                        ),
                    ),
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }


}
