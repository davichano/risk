<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:01 PM
 */

namespace Proyecto\Model;

use Zend\Db\TableGateway\TableGateway;


class ProyectocontrolTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $adapter = $this->tableGateway->getAdapter();
        $projectTable = new TableGateway('proyecto_control', $adapter);
        $this->tableGateway = $projectTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function  historyProject($id)
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'fecha',
                'estado',
            )
        );
        $sqlSelect->join(
            'control',
            'control.idcontrol = proyecto_control.control_idcontrol',
            array(
                'idcontrol'=>'idcontrol',
                'nombre' => 'nombre',
                'descripcion' => 'descripcion',
            )
        );
//        $sqlSelect->order('idproyecto DESC');
        $sqlSelect->where(
            array("proyecto_control.proyecto_idproyecto = $id")
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    public function getProyectoControl($idproyecto, $idcontrol)
    {
        $rowset = $this->tableGateway->select(
            array('proyecto_idproyecto' => $idproyecto,
                  'control_idcontrol'   => $idcontrol)
        );
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function enableProyectoControl($idproyecto, $idcontrol)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('proyecto_idproyecto' => $idproyecto,
                                          'control_idcontrol'   => $idcontrol)
        );
    }

    public function disableProyectoControl($idproyecto, $idcontrol)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('proyecto_idproyecto' => $idproyecto,
                                          'control_idcontrol'   => $idcontrol)
        );
    }

    public function saveProyectoControl(Proyectocontrol $proyecto)
    {
        $data = array(
            'fecha' => $proyecto->fecha,
            'estado' => $proyecto->estado,
            'proyecto_idproyecto' => $proyecto->proyecto_idproyecto,
            'control_idcontrol' => $proyecto->control_idcontrol,
        );

        $idproyecto = (int)$proyecto->proyecto_idproyecto;
        $idcontrol = (int)$proyecto->control_idcontrol;
        if ($this->getProyectoControl($idproyecto,$idcontrol)) {
            $this->tableGateway->update(
                $data, array('proyecto_idproyecto' => $idproyecto,'control_idcontrol'=>$idcontrol)
            );
        }else{
            $this->tableGateway->insert($data);
        }
    }

}