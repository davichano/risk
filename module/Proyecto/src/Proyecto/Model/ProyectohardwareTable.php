<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:01 PM
 */

namespace Proyecto\Model;

use Zend\Db\TableGateway\TableGateway;


class ProyectohardwareTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $adapter = $this->tableGateway->getAdapter();
        $projectTable = new TableGateway('proyecto_hardware', $adapter);
        $this->tableGateway = $projectTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function  historyProject($id)
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'fecha',
                'estado',
            )
        );
        $sqlSelect->join(
            'hardware',
            'hardware.idhardware = proyecto_hardware.hardware_idhardware',
            array(
                'idhardware'=>'idhardware',
                'nombre' => 'nombre',
                'descripcion' => 'descripcion',
            )
        );
//        $sqlSelect->order('idproyecto DESC');
        $sqlSelect->where(
            array("proyecto_hardware.proyecto_idproyecto = $id")
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    public function getProyectoHardware($idproyecto, $idhardware)
    {
        $rowset = $this->tableGateway->select(
            array('proyecto_idproyecto' => $idproyecto,
                  'hardware_idhardware'   => $idhardware)
        );
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function enableProyectoHardware($idproyecto, $idhardware)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('proyecto_idproyecto' => $idproyecto,
                                          'hardware_idhardware'   => $idhardware)
        );
    }

    public function disableProyectoHardware($idproyecto, $idhardware)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('proyecto_idproyecto' => $idproyecto,
                                          'hardware_idhardware'   => $idhardware)
        );
    }

    public function saveProyectoHardware(Proyectohardware $proyecto)
    {
        $data = array(
            'fecha' => $proyecto->fecha,
            'estado' => $proyecto->estado,
            'proyecto_idproyecto' => $proyecto->proyecto_idproyecto,
            'hardware_idhardware' => $proyecto->hardware_idhardware,
        );

        $idproyecto = (int)$proyecto->proyecto_idproyecto;
        $idhardware = (int)$proyecto->hardware_idhardware;
        if ($this->getProyectoHardware($idproyecto,$idhardware)) {
            $this->tableGateway->update(
                $data, array('proyecto_idproyecto' => $idproyecto,'hardware_idhardware'=>$idhardware)
            );
        }else{
            $this->tableGateway->insert($data);
        }
    }

}