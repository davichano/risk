<?php
namespace Proyecto\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class ProyectoTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function  fetchAllInner()
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'idproyecto',
                'nombre',
                'descripcion',
                'mision',
                'datacritica',
                'almacenamiento',
                'seguridad',
                'estado',
            )
        );
        $sqlSelect->join(
            'tipo',
            'proyecto.tipo_idtipo = tipo.idtipo',
            array(
                'tipo' => 'nombre',
            )
        );
        $sqlSelect->order('idproyecto DESC');
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getProyecto($id)
    {
        $rowset = $this->tableGateway->select(array('idproyecto' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No se encontro datos en: $id");
        }

        return $row;
    }

    public function enableProyecto($id)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('idproyecto' => $id)
        );
    }

    public function disableProyecto($id)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('idproyecto' => $id)
        );
    }

    public function saveProyecto(Proyecto $proyecto)
    {
        $data = array(
            'nombre'         => $proyecto->nombre,
            'descripcion'    => $proyecto->descripcion,
            'mision'         => $proyecto->mision,
            'datacritica'    => $proyecto->datacritica,
            'almacenamiento' => $proyecto->almacenamiento,
            'seguridad'      => $proyecto->seguridad,
            'estado'         => $proyecto->estado,
        );

        $id = (int)$proyecto->idproyecto;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getProyecto($id)) {
                $this->tableGateway->update(
                    $data, array('idproyecto' => $id)
                );
            } else {
                throw new \Exception('Proyecto no existe');
            }
        }

        return $id;
    }

    public function deleteProyecto($id)
    {
        $this->tableGateway->delete(array('idproyecto' => $id));
    }


}

?>