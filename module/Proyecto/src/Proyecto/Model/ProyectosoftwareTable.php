<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:01 PM
 */

namespace Proyecto\Model;

use Zend\Db\TableGateway\TableGateway;


class ProyectosoftwareTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $adapter = $this->tableGateway->getAdapter();
        $projectTable = new TableGateway('proyecto_software', $adapter);
        $this->tableGateway = $projectTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function  historyProject($id)
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'fecha',
                'estado',
            )
        );
        $sqlSelect->join(
            'software',
            'software.idsoftware = proyecto_software.software_idsoftware',
            array(
                'idsoftware'=>'idsoftware',
                'nombre' => 'nombre',
                'descripcion' => 'descripcion',
            )
        );
//        $sqlSelect->order('idproyecto DESC');
        $sqlSelect->where(
            array("proyecto_software.proyecto_idproyecto = $id")
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    public function getProyectoSoftware($idproyecto, $idsoftware)
    {
        $rowset = $this->tableGateway->select(
            array('proyecto_idproyecto' => $idproyecto,
                  'software_idsoftware'   => $idsoftware)
        );
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function enableProyectoSoftware($idproyecto, $idsoftware)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('proyecto_idproyecto' => $idproyecto,
                                          'software_idsoftware'   => $idsoftware)
        );
    }

    public function disableProyectoSoftware($idproyecto, $idsoftware)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('proyecto_idproyecto' => $idproyecto,
                                          'software_idsoftware'   => $idsoftware)
        );
    }

    public function saveProyectoSoftware(Proyectosoftware $proyecto)
    {
        $data = array(
            'fecha' => $proyecto->fecha,
            'estado' => $proyecto->estado,
            'proyecto_idproyecto' => $proyecto->proyecto_idproyecto,
            'software_idsoftware' => $proyecto->software_idsoftware,
        );

        $idproyecto = (int)$proyecto->proyecto_idproyecto;
        $idsoftware = (int)$proyecto->software_idsoftware;
        if ($this->getProyectoSoftware($idproyecto,$idsoftware)) {
            $this->tableGateway->update(
                $data, array('proyecto_idproyecto' => $idproyecto,'software_idsoftware'=>$idsoftware)
            );
        }else{
            $this->tableGateway->insert($data);
        }
    }

}