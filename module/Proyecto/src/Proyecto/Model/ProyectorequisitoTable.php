<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:01 PM
 */

namespace Proyecto\Model;

use Zend\Db\TableGateway\TableGateway;


class ProyectorequisitoTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $adapter = $this->tableGateway->getAdapter();
        $projectTable = new TableGateway('proyecto_requisito', $adapter);
        $this->tableGateway = $projectTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function  historyProject($id)
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'fecha',
                'estado',
            )
        );
        $sqlSelect->join(
            'requisito',
            'requisito.idrequisito = proyecto_requisito.requisito_idrequisito',
            array(
                'idrequisito'=>'idrequisito',
                'nombre' => 'nombre',
                'descripcion' => 'descripcion',
            )
        );
//        $sqlSelect->order('idproyecto DESC');
        $sqlSelect->where(
            array("proyecto_requisito.proyecto_idproyecto = $id")
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    public function getProyectoRequisito($idproyecto, $idrequisito)
    {
        $rowset = $this->tableGateway->select(
            array('proyecto_idproyecto' => $idproyecto,
                  'requisito_idrequisito'   => $idrequisito)
        );
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function enableProyectoRequisito($idproyecto, $idrequisito)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('proyecto_idproyecto' => $idproyecto,
                                          'requisito_idrequisito'   => $idrequisito)
        );
    }

    public function disableProyectoRequisito($idproyecto, $idrequisito)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('proyecto_idproyecto' => $idproyecto,
                                          'requisito_idrequisito'   => $idrequisito)
        );
    }

    public function saveProyectoRequisito(Proyectorequisito $proyecto)
    {
        $data = array(
            'fecha' => $proyecto->fecha,
            'estado' => $proyecto->estado,
            'proyecto_idproyecto' => $proyecto->proyecto_idproyecto,
            'requisito_idrequisito' => $proyecto->requisito_idrequisito,
        );

        $idproyecto = (int)$proyecto->proyecto_idproyecto;
        $idrequisito = (int)$proyecto->requisito_idrequisito;
        if ($this->getProyectoRequisito($idproyecto,$idrequisito)) {
            $this->tableGateway->update(
                $data, array('proyecto_idproyecto' => $idproyecto,'requisito_idrequisito'=>$idrequisito)
            );
        }else{
            $this->tableGateway->insert($data);
        }
    }

}