<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:01 PM
 */

namespace Proyecto\Model;

use Zend\Db\TableGateway\TableGateway;


class ProyectotecnicaTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $adapter = $this->tableGateway->getAdapter();
        $projectTable = new TableGateway('proyecto_tecnica', $adapter);
        $this->tableGateway = $projectTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function  historyProject($id)
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'fecha',
                'estado',
            )
        );
        $sqlSelect->join(
            'tecnica',
            'tecnica.idtecnica = proyecto_tecnica.tecnica_idtecnica',
            array(
                'idtecnica'=>'idtecnica',
                'nombre' => 'nombre',
                'descripcion' => 'descripcion',
            )
        );
//        $sqlSelect->order('idproyecto DESC');
        $sqlSelect->where(
            array("proyecto_tecnica.proyecto_idproyecto = $id")
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    public function getProyectoTecnica($idproyecto, $idtecnica)
    {
        $rowset = $this->tableGateway->select(
            array('proyecto_idproyecto' => $idproyecto,
                  'tecnica_idtecnica'   => $idtecnica)
        );
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function enableProyectoTecnica($idproyecto, $idtecnica)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('proyecto_idproyecto' => $idproyecto,
                                          'tecnica_idtecnica'   => $idtecnica)
        );
    }

    public function disableProyectoTecnica($idproyecto, $idtecnica)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('proyecto_idproyecto' => $idproyecto,
                                          'tecnica_idtecnica'   => $idtecnica)
        );
    }

    public function saveProyectoTecnica(Proyectotecnica $proyecto)
    {
        $data = array(
            'fecha' => $proyecto->fecha,
            'estado' => $proyecto->estado,
            'proyecto_idproyecto' => $proyecto->proyecto_idproyecto,
            'tecnica_idtecnica' => $proyecto->tecnica_idtecnica,
        );

        $idproyecto = (int)$proyecto->proyecto_idproyecto;
        $idtecnica = (int)$proyecto->tecnica_idtecnica;
        if ($this->getProyectoTecnica($idproyecto,$idtecnica)) {
            $this->tableGateway->update(
                $data, array('proyecto_idproyecto' => $idproyecto,'tecnica_idtecnica'=>$idtecnica)
            );
        }else{
            $this->tableGateway->insert($data);
        }
    }

}