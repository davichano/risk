<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:01 PM
 */

namespace Proyecto\Model;

use Zend\Db\TableGateway\TableGateway;


class ProyectovulnerabilidadTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $adapter = $this->tableGateway->getAdapter();
        $projectTable = new TableGateway('proyecto_vulnerabilidad', $adapter);
        $this->tableGateway = $projectTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function  historyProject($id)
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'fecha',
                'estado',
                'impacto',
            )
        );
        $sqlSelect->join(
            'vulnerabilidad',
            'vulnerabilidad.idvulnerabilidad = proyecto_vulnerabilidad.vulnerabilidad_idvulnerabilidad',
            array(
                'idvulnerabilidad'=>'idvulnerabilidad',
                'nombre' => 'nombre',
                'descripcion' => 'descripcion',
            )
        );
//        $sqlSelect->order('idproyecto DESC');
        $sqlSelect->where(
            array("proyecto_vulnerabilidad.proyecto_idproyecto = $id")
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    public function getProyectoVulnerabilidad($idproyecto, $idvulnerabilidad)
    {
        $rowset = $this->tableGateway->select(
            array('proyecto_idproyecto' => $idproyecto,
                  'vulnerabilidad_idvulnerabilidad'   => $idvulnerabilidad)
        );
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function enableProyectoVulnerabilidad($idproyecto, $idvulnerabilidad)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('proyecto_idproyecto' => $idproyecto,
                                          'vulnerabilidad_idvulnerabilidad'   => $idvulnerabilidad)
        );
    }

    public function disableProyectoVulnerabilidad($idproyecto, $idvulnerabilidad)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('proyecto_idproyecto' => $idproyecto,
                                          'vulnerabilidad_idvulnerabilidad'   => $idvulnerabilidad)
        );
    }

    public function saveProyectoVulnerabilidad(Proyectovulnerabilidad $proyecto)
    {
        $data = array(
            'fecha' => $proyecto->fecha,
            'estado' => $proyecto->estado,
            'impacto' => $proyecto->impacto,
            'proyecto_idproyecto' => $proyecto->proyecto_idproyecto,
            'vulnerabilidad_idvulnerabilidad' => $proyecto->vulnerabilidad_idvulnerabilidad,
        );

        $idproyecto = (int)$proyecto->proyecto_idproyecto;
        $idvulnerabilidad = (int)$proyecto->vulnerabilidad_idvulnerabilidad;
        if ($this->getProyectoVulnerabilidad($idproyecto,$idvulnerabilidad)) {
            $this->tableGateway->update(
                $data, array('proyecto_idproyecto' => $idproyecto,'vulnerabilidad_idvulnerabilidad'=>$idvulnerabilidad)
            );
        }else{
            $this->tableGateway->insert($data);
        }
    }

}