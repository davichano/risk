<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:01 PM
 */

namespace Proyecto\Model;

use Zend\Db\TableGateway\TableGateway;


class ProyectoriesgoTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $adapter = $this->tableGateway->getAdapter();
        $projectTable = new TableGateway('proyecto_riesgo', $adapter);
        $this->tableGateway = $projectTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function  historyProject($id)
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'fecha',
                'estado',
                'valor',
            )
        );
        $sqlSelect->join(
            'riesgo',
            'riesgo.idriesgo = proyecto_riesgo.riesgo_idriesgo',
            array(
                'idriesgo'=>'idriesgo',
                'nombre' => 'nombre',
                'descripcion' => 'descripcion',
            )
        );
        $sqlSelect->join(
            'tipo',
            'tipo.idtipo = riesgo.tipo_idtipo',
            array(
                'tipo' => 'nombre',
            )
        );
//        $sqlSelect->order('idproyecto DESC');
        $sqlSelect->where(
            array("proyecto_riesgo.proyecto_idproyecto = $id")
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    public function getProyectoRiesgo($idproyecto, $idriesgo)
    {
        $rowset = $this->tableGateway->select(
            array('proyecto_idproyecto' => $idproyecto,
                  'riesgo_idriesgo'   => $idriesgo)
        );
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function enableProyectoRiesgo($idproyecto, $idriesgo)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('proyecto_idproyecto' => $idproyecto,
                                          'riesgo_idriesgo'   => $idriesgo)
        );
    }

    public function disableProyectoRiesgo($idproyecto, $idriesgo)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('proyecto_idproyecto' => $idproyecto,
                                          'riesgo_idriesgo'   => $idriesgo)
        );
    }

    public function saveProyectoRiesgo(Proyectoriesgo $proyecto)
    {
        $data = array(
            'fecha' => $proyecto->fecha,
            'estado' => $proyecto->estado,
            'valor' => $proyecto->valor,
            'proyecto_idproyecto' => $proyecto->proyecto_idproyecto,
            'riesgo_idriesgo' => $proyecto->riesgo_idriesgo,
        );

        $idproyecto = (int)$proyecto->proyecto_idproyecto;
        $idriesgo = (int)$proyecto->riesgo_idriesgo;
        if ($this->getProyectoRiesgo($idproyecto,$idriesgo)) {
            $this->tableGateway->update(
                $data, array('proyecto_idproyecto' => $idproyecto,'riesgo_idriesgo'=>$idriesgo)
            );
        }else{
            $this->tableGateway->insert($data);
        }
    }

}