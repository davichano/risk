<?php
/**
 * Created by PhpStorm.
 * User: David Paredes
 * Date: 15/12/2015
 * Time: 10:00 PM
 */

namespace Proyecto\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Proyectoriesgo implements InputFilterAwareInterface
{
    public $proyecto_idproyecto;
    public $riesgo_idriesgo;
    public $fecha;
    public $valor;
    public $estado;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->proyecto_idproyecto = (!empty($data['proyecto_idproyecto'])) ? $data['proyecto_idproyecto'] : null;
        $this->riesgo_idriesgo = (!empty($data['riesgo_idriesgo'])) ? $data['riesgo_idriesgo'] : null;
        $this->fecha = (!empty($data['fecha'])) ? $data['fecha'] : null;
        $this->valor = (!empty($data['valor'])) ? $data['valor'] : null;
        $this->estado = (!empty($data['estado'])) ? $data['estado'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(
                array(
                    'name'       => 'proyecto_idproyecto',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Seleccione un tipo proyecto"
                                ),
                            )
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'riesgo_idriesgo',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Seleccione una técnica"
                                ),
                            )
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'valor',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Seleccione una valor"
                                ),
                            )
                        ),
                    ),
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}