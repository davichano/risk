<?php
namespace Software\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class SoftwareTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getSoftware($id)
    {
        $rowset = $this->tableGateway->select(array('idsoftware' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No se encontro datos en: $id");
        }

        return $row;
    }

    public function enableSoftware($id)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('idsoftware' => $id)
        );
    }

    public function disableSoftware($id)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('idsoftware' => $id)
        );
    }

    public function saveSoftware(Software $software)
    {
        $data = array(
            'nombre' => $software->nombre,
            'descripcion' => $software->descripcion,
            'estado' => $software->estado,
        );

        $id = (int)$software->idsoftware;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getSoftware($id)) {
                $this->tableGateway->update(
                    $data, array('idsoftware' => $id)
                );
            } else {
                throw new \Exception('Software no existe');
            }
        }

        return $id;
    }

    public function deleteSoftware($id)
    {
        $this->tableGateway->delete(array('idsoftware' => $id));
    }


}

?>