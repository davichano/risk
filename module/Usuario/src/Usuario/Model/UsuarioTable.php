<?php
namespace Usuario\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class UsuarioTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getUsuario($id)
    {
        $rowset = $this->tableGateway->select(array('idusuario' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No se encontro datos en: $id");
        }

        return $row;
    }

    public function enableUsuario($id)
    {
        $this->tableGateway->update(
            array("estado" => "1"), array('idusuario' => $id)
        );
    }

    public function disableUsuario($id)
    {
        $this->tableGateway->update(
            array("estado" => "0"), array('idusuario' => $id)
        );
    }

    public function saveUsuario(Usuario $usuario)
    {
        $data = array(
            'nombre' => $usuario->nombre,
            'descripcion' => $usuario->descripcion,
            'estado' => $usuario->estado,
        );

        $id = (int)$usuario->idusuario;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getUsuario($id)) {
                $this->tableGateway->update(
                    $data, array('idusuario' => $id)
                );
            } else {
                throw new \Exception('Usuario no existe');
            }
        }

        return $id;
    }

    public function deleteUsuario($id)
    {
        $this->tableGateway->delete(array('idusuario' => $id));
    }


}

?>