<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Usuario\Controller;

use Usuario\Form\UsuarioForm;
use Usuario\Model\Usuario;
use Zend\Db\Sql\Sql;
use Zend\Form\FormInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    protected $regionTable;

    public function indexAction()
    {
        return new ViewModel(
            array(
                'data' => $this->getUsuarioTable()->fetchAll(),
            )
        );
    }

    public function addAction()
    {
        $form = new UsuarioForm('usuario-form');
        $form->get('estado')->setValue('1');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $usuario = new Usuario();
            $form->setInputFilter($usuario->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $usuario->exchangeArray($form->getData());
                $this->getUsuarioTable()
                    ->saveUsuario(
                        $usuario
                    );

                return $this->redirect()->toRoute(
                    'usuario',
                    array('action' => 'index')
                );
            }
        }

        return array('form' => $form);

    }

    public function editAction()
    {

        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'usuario', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $usuario = $this->getUsuarioTable()->getUsuario($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'usuario', array(
                    'action' => 'index'
                )
            );
        }

        $form = new UsuarioForm();
        $form->bind($usuario);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($usuario->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $usuario->idusuario=$id;
                $this->getUsuarioTable()->saveUsuario($usuario);
//                 return new JsonModel(array("t"=>$usuario));
                return $this->redirect()->toRoute('usuario');
            }
        }

        return array(
            'id'   => $id,
            'form' => $form,
        );

    }

    public function updateAction()
    {

        if ($this->request->isXmlHttpRequest()) {
            $f = $this->request->getPost('f');

            switch ($f) {
                case "1":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getUsuarioTable()->disableUsuario($value);
                    }
                    break;
                case "2":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getUsuarioTable()->enableUsuario($value);
                    }
                    break;

                default:
                    # code...
                    break;
            }

            return new JsonModel(array('d' => $f));
            // return $s;
        } else {
            return new JsonModel(array('d' => '1'));
        }/*
      echo "<script>alert('SSS');</script>";
        return new JsonModel($this->getUsuarioTable()->fetchAll());*/


    }


    public function getUsuarioTable()
    {
        if (!$this->regionTable) {
            $sm = $this->getServiceLocator();
            $this->regionTable = $sm->get('Usuario\Model\UsuarioTable');
        }

        return $this->regionTable;
    }


    public function resultAction()
    {
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select()
            ->from('provincia')
            ->where(array('IdProvincia' => 1))
            ->order('nombre DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        //$datos=$result->toArray();
        $toRet = array('nom'   => "David", 'ape' => "Paredes",
                       'datos' => $result);

        return new ViewModel($toRet);

    }


}
