<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class IndexController extends AbstractActionController
{

	public $dbAdapter;

    public function indexAction()
    {
        return new ViewModel();
    }

    public function resultAction()
    {
    	
    	$this->dbAdapter=$this->getServiceLocator()->get('Zend\Db\Adapter');
    	/*
    	$result=$this->dbAdapter->query("select * from tecnica ",Adapter::QUERY_MODE_EXECUTE);
    	$datos=$result->toArray();
    	*/
    	$sql=new Sql($this->dbAdapter);
    	$select = $sql->select()
    		->from('provincia')
    		->where(array('IdProvincia'=>1))
    		->order('nombre DESC');
    	$statement=$sql->prepareStatementForSqlObject($select);
    	$result=$statement->execute();
    	//$datos=$result->toArray();
    	$toRet = array('nom' =>"David" ,'ape'=>"Paredes",'datos'=>$result);
        return new ViewModel($toRet);
    }

}
