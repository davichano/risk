<?php
namespace Control\Form;

use Zend\Form\Form;

/**
 *
 */
class ControlForm extends Form
{

    function __construct($name = null)
    {
        parent::__construct($name = null);

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'control-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(
            array(
                'name' => 'idcontrol',
                'type' => 'Hidden',
            )
        );

        $this->add(
            array(
                'name'       => 'nombre',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ingrese el nombre',
                    'id'             => 'nombre',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Controles servirán para reducir o eliminar el impacto de un riesgo',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'descripcion',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Ingrese una descripción',
                    'id'          => 'descripcion',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Detalle el control para poder utilizarlo correctamente',
                ),
            )
        );
        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Radio',
                'name'       => 'estado',
                'options'    => array(
                    'disable_inarray_validator' => true, // <-- disable
                    'label_attributes' => array(
                        'class' => 'radio-inline',
                    ),
                    'value_options'    => array(
                        '1' => 'Implementado',
                        '2' => 'Por implementar'
                    ),
                ),
                'attributes' => array(
//                    'value' => '1',
                ),
            )
        );
    }
}