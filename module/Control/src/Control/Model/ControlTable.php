<?php 
namespace Control\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

/**
 * 
 */
 class ControlTable
 {
 	  protected $tableGateway;
    protected $dbAdapter;

 	public function __construct(TableGateway $tableGateway)
     {
         $this->tableGateway = $tableGateway;
     }

 	public function fetchAll()
     {
         $resultSet = $this->tableGateway->select();
         return $resultSet;
     }
     public function getControl($id){
     	 $rowset = $this->tableGateway->select(array('idcontrol' => $id));
         $row = $rowset->current();
         if (!$row) {
             throw new \Exception("No se encontro datos en: $id");
         }
         return $row;
     }

     public function enableControl($id){
        $this->tableGateway->update(array("estado"=>"1"),array('idcontrol' => $id));
     }

    public function disableControl($id){
        $this->tableGateway->update(array("estado"=>"0"),array('idcontrol' => $id));
     }

     public function saveControl(Control $control)
     {
         $data = array(
             'nombre'  => $control->nombre,
             'descripcion' => $control->descripcion,
             'estado' => $control->estado,
         );

         $id = (int) $control->idcontrol;
         if ($id == 0) {
             $this->tableGateway->insert($data);
             $id=$this->tableGateway->lastInsertValue;
         } else {
             if ($this->getControl($id)) {
                 $this->tableGateway->update($data, array('idcontrol' => $id));
             } else {
                 throw new \Exception('Región no existe');
             }
         }
         return $id;
     }

     public function deleteControl($id)
     {
         $this->tableGateway->delete(array('idcontrol' => $id));
     }


 } 

 ?>