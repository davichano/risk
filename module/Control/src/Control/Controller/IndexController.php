<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Control\Controller;

use Control\Form\ControlForm;
use Control\Model\Control;
use Zend\Db\Sql\Sql;
use Zend\Form\FormInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    protected $regionTable;

    public function indexAction()
    {
        return new ViewModel(
            array(
                'data' => $this->getControlTable()->fetchAll(),
            )
        );
    }

    public function addAction()
    {
        $form = new ControlForm('control-form');
//        $form->get('estado')->setValue('1');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $control = new Control();
            $form->setInputFilter($control->getInputFilter());
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
//            return new JsonModel(array("d"=>$form->getData()));

            if ($form->isValid()) {

                $control->exchangeArray($form->getData());
                $this->getControlTable()
                    ->saveControl(
                        $control
                    );

                return $this->redirect()->toRoute(
                    'control',
                    array('action' => 'index')
                );
            }

        }

        return array('form' => $form);

    }

    public function editAction()
    {

        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'control', array(
                    'action' => 'add'
                )
            );
        }
        try {
            $control = $this->getControlTable()->getControl($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'control', array(
                    'action' => 'index'
                )
            );
        }

        $form = new ControlForm();
        $form->bind($control);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($control->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $control->idcontrol=$id;
                $this->getControlTable()->saveControl($control);
                return $this->redirect()->toRoute('control');
            }
        }

        return array(
            'id'   => $id,
            'form' => $form,
        );

    }

    public function updateAction()
    {

        if ($this->request->isXmlHttpRequest()) {
            $f = $this->request->getPost('f');

            switch ($f) {
                case "1":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getControlTable()->disableControl($value);
                    }
                    break;
                case "2":
                    $arr = $this->request->getPost('v');
                    foreach ($arr as $key => $value) {
                        $this->getControlTable()->enableControl($value);
                    }
                    break;

                default:
                    # code...
                    break;
            }

            return new JsonModel(array('d' => $f));
            // return $s;
        } else {
            return new JsonModel(array('d' => '1'));
        }/*
      echo "<script>alert('SSS');</script>";
        return new JsonModel($this->getControlTable()->fetchAll());*/


    }


    public function getControlTable()
    {
        if (!$this->regionTable) {
            $sm = $this->getServiceLocator();
            $this->regionTable = $sm->get('Control\Model\ControlTable');
        }

        return $this->regionTable;
    }


    public function resultAction()
    {
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select()
            ->from('provincia')
            ->where(array('IdProvincia' => 1))
            ->order('nombre DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        //$datos=$result->toArray();
        $toRet = array('nom'   => "David", 'ape' => "Paredes",
                       'datos' => $result);

        return new ViewModel($toRet);

    }


}
